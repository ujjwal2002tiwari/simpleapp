//jshint esversion:6
const express=require("express");
const bodyParser= require("body-parser");

const app=express();
app.use(bodyParser.urlencoded({extended: true}));
app.get("/",function(req,res){
  // res.sendfile("index.html") we will not send this as in cloud the data is not tha organised
  res.sendFile(__dirname + "/bmiCalculator.html");//dirname will give the current file location

});
app.post("/bmicalculator",function(req,res){

  console.log(req.body);
  var weight=Number(req.body.weight);
  var height=Number(req.body.height);

  var result=weight/(height*height);

  res.send("Your BMI is= "+result);
});
app.listen(3000,function(){
  console.log("calculator started at port 3000");
});
